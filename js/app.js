/* =======================================================================
 This file is created by Chechu Castro [chechu@digitatis.com]
 In order to use this code (or a part of it), please feel free
 to contact me.
 Website : http://www.digitatis.com
 Twitter : nexus5_com_es
 FILE NAME: main.js - Copyright (C)
======================================================================= */
$(function() {
  var myObj = {
    init: function(){
      var _this = this;
      // Activate modules
      this.moduleOne.init();
    },
    moduleOne: {
      init: function(){
        this._MyMethodHere();
      },
      _MyMethodHere: function(){
        console.log('This is the main JS file');
      }
    }
  };

  // Init
  myObj.init();
});
