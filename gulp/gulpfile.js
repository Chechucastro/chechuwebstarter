/* =======================================================================
 This file is created by Chechu Castro [chechu@digitatis.com]
 In order to use this code (or a part of it), please feel free
 to contact me.
 Website : http://www.digitatis.com
 Twitter : nexus5_com_es
 FILE NAME: gulpfile.js - Copyright (C)
======================================================================= */

// Include gulp
var gulp      = require('gulp'),
  // Include Our Plugins
  jshint      = require('gulp-jshint'),
  sass        = require('gulp-sass'),
  concat      = require('gulp-concat'),
  uglify      = require('gulp-uglify'),
  watch       = require('gulp-watch'),
  rename      = require('gulp-rename'),
  imagemin    = require('gulp-imagemin'),
  cache       = require('gulp-cache'),
  livereload  = require('gulp-livereload');
  notify      = require('gulp-notify'),
  neat        = require('node-neat').includePaths,
  cssmin      = require('gulp-cssmin');


// Lint Task
gulp.task('lint', function() {
    return gulp.src('../js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(notify({ message: 'Lint scripts task complete' }));
});
// Html
gulp.task('html', function() {
    return gulp.src('../dist/html/*.html')
        .pipe(gulp.dest('../dist/html'))
        .pipe(livereload())
        .pipe(notify({ message: 'Html task complete' }));
});
// Compile Sass
gulp.task('sass', function() {
    return gulp.src('../sass/*.scss')
        .pipe(sass({
            includePaths: ['stylesheet'].concat(neat)
        }))
        .pipe(gulp.dest('../dist/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('../dist/css'))
        .pipe(livereload())
        .pipe(notify({ message: 'Sass task complete' }));
});

// Images optimization
gulp.task('images', function() {
  return gulp.src('../dist/images')
        .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
        .pipe(gulp.dest('../dist/images/optimized/'))
        .pipe(notify({ message: 'Images are minified!' }));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(['../js/libs/zepto/zepto.min.js', '../js/libs/zepto/zepto.touch.js','../js/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('../dist/js'))
        .pipe(rename('app.min.js'))
        .pipe(uglify())
        .pipe(livereload())
        .pipe(gulp.dest('../dist/js'))
        .pipe(notify({ message: 'Scripts task complete' }));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('../js/*.js', ['lint', 'scripts']);
    gulp.watch('../sass/*.scss', ['sass']);
    gulp.watch('../images/**/*', ['images']);
    gulp.watch('../dist/html/*.html', ['html']);
});

// Default Task
gulp.task('default', ['watch']);
